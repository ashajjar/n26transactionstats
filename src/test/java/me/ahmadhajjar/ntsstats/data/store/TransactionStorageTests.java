package me.ahmadhajjar.ntsstats.data.store;

import me.ahmadhajjar.ntsstats.data.Transaction;
import org.junit.Before;
import org.junit.FixMethodOrder;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.junit.runners.MethodSorters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import static java.lang.Thread.sleep;
import static org.junit.Assert.assertEquals;

/**
 * @author Ahmad Hajjar <ash852006@gmail.com>
 * Created on 08/06/2018
 */

@RunWith(SpringRunner.class)
@SpringBootTest
@TestExecutionListeners(listeners = DependencyInjectionTestExecutionListener.class)
@FixMethodOrder(MethodSorters.NAME_ASCENDING)
public class TransactionStorageTests {

    @Autowired
    private TransactionStorage storage;

    @Before
    public void beforeTest() {
        storage.getTransactionsStatistics().reset();
    }

    @Test
    public void testTransactionStorageIsEmptyInTheBeginning() {
        TransactionsStatistics statistics = storage.getTransactionsStatistics();
        assertEquals(0.0, statistics.getAvg(), 0.0);
        assertEquals(0.0, statistics.getSum(), 0.0);
        assertEquals(0L, statistics.getCount(), 0.0);
        assertEquals(Double.MIN_VALUE, statistics.getMax(), 0.0);
        assertEquals(Double.MAX_VALUE, statistics.getMin(), 0.0);
    }

    @Test
    public void testStorageWillReturnStatsForOneTransaction() {
        Transaction transaction = new Transaction(Math.random(), getFutureTimestamp());
        storage.addTransaction(transaction);
        TransactionsStatistics statistics = storage.getTransactionsStatistics();
        assertEquals(statistics.getSum(), transaction.getAmount(), 0);
        assertEquals(statistics.getAvg(), transaction.getAmount(), 0);
        assertEquals(statistics.getMin(), transaction.getAmount(), 0);
        assertEquals(statistics.getMax(), transaction.getAmount(), 0);
        assertEquals(statistics.getCount(), 1L, 0.0);
    }

    @Test
    public void testStorageWillReturnCorrectStatsForMultipleTransactions() {
        double sum = 0.0, min = Double.MAX_VALUE, max = Double.MIN_VALUE;
        long count = 10;
        for (int i = 0; i < count; i++) {
            double amount = Math.random();
            sum += amount;
            if (amount < min)
                min = amount;
            if (amount > max)
                max = amount;
            Transaction transaction = new Transaction(amount, getFutureTimestamp());
            storage.addTransaction(transaction);
        }

        final TransactionsStatistics statistics = storage.getTransactionsStatistics();
        assertEquals(statistics.getSum(), sum, 0);
        assertEquals(statistics.getAvg(), sum / count, 0);
        assertEquals(statistics.getMin(), min, 0);
        assertEquals(statistics.getMax(), max, 0);
        assertEquals(statistics.getCount(), count, 0.0);
    }

    @Test(expected = IllegalStateException.class)
    public void testTransactionStorageWillThrowExceptionForOldTransactions() {
        storage.addTransaction(new Transaction(Math.random(), getObsoleteTimestamp()));
    }

    private long getObsoleteTimestamp() {
        return System.currentTimeMillis() - 120000;
    }

    private long getFiftyFiveSecondsOldTimestamp() {
        return System.currentTimeMillis() - 55000;
    }

    private long getFutureTimestamp() {
        return System.currentTimeMillis() + 360000;
    }

    @Test
    public void testStorageWillBeEmptyAfterSomeTime() throws InterruptedException {
        for (int i = 0; i < 10; i++) {
            Transaction transaction = new Transaction(Math.random(), getFiftyFiveSecondsOldTimestamp());
            storage.addTransaction(transaction);
        }
        assertEquals(storage.getTransactionsStatistics().getCount(), 10, 0.0);
        sleep(5500);
        assertEquals(storage.getTransactionsStatistics().getCount(), 0, 0.0);
    }

}
