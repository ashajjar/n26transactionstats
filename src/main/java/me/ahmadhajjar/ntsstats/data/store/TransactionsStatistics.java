package me.ahmadhajjar.ntsstats.data.store;

/**
 * @author Ahmad Hajjar <ash852006@gmail.com>
 * Created on 08/06/2018
 */
public class TransactionsStatistics {
    private Double sum = 0.0;
    private Double avg = 0.0;
    private Double max = Double.MIN_VALUE;
    private Double min = Double.MAX_VALUE;
    private Long count = 0L;

    public Double getSum() {
        return sum;
    }

    public synchronized void setSum(Double sum) {
        this.sum = sum;
    }

    public Double getAvg() {
        return avg;
    }

    public synchronized void setAvg(Double avg) {
        this.avg = avg;
    }

    public Double getMax() {
        return max;
    }

    public synchronized void setMax(Double max) {
        this.max = max;
    }

    public Double getMin() {
        return min;
    }

    public synchronized void setMin(Double min) {
        this.min = min;
    }

    public Long getCount() {
        return count;
    }

    public synchronized void setCount(Long count) {
        this.count = count;
    }

    public synchronized void update(Double amount) {
        setSum(getSum() + amount);
        setCount(getCount() + 1);
        if (getCount() > 0) {
            setAvg(getSum() / getCount());
        } else {
            setAvg(0.0);
            setMin(Double.MAX_VALUE);
            setMax(Double.MIN_VALUE);
            return;
        }

        if (amount > getMax()) {
            setMax(amount);
        }

        if (amount < getMin()) {
            setMin(amount);
        }
    }

    public void reset() {
        setSum(0.0);
        setCount(0L);
        setAvg(0.0);
        setMin(Double.MAX_VALUE);
        setMax(Double.MIN_VALUE);
    }
}
