package me.ahmadhajjar.ntsstats.data.store;

import me.ahmadhajjar.ntsstats.config.Config;
import me.ahmadhajjar.ntsstats.data.Transaction;
import net.jodah.expiringmap.ExpiringMap;
import org.springframework.stereotype.Service;

import java.util.Collections;
import java.util.UUID;
import java.util.concurrent.TimeUnit;

/**
 * @author Ahmad Hajjar <ash852006@gmail.com>
 * Created on 08/06/2018
 */
@Service
public class TransactionStorage {
    private ExpiringMap<String, Transaction> transactions;

    private TransactionsStatistics transactionsStatistics = new TransactionsStatistics();

    public TransactionStorage() {

        transactions = ExpiringMap.builder()
                .maxSize(Math.toIntExact(Config.WINDOW))
                .variableExpiration()
                .expirationListener((key, transaction) -> {
                    if (transactions.size() == 0) {
                        transactionsStatistics.reset();
                        return;
                    }
                    Double amount = ((Transaction) transaction).getAmount();
                    transactionsStatistics.setSum(transactionsStatistics.getSum() - amount);
                    transactionsStatistics.setCount(transactionsStatistics.getCount() - 1);
                    transactionsStatistics.setAvg(transactionsStatistics.getSum() / transactionsStatistics.getCount());

                    if (amount.equals(transactionsStatistics.getMin())) {
                        transactionsStatistics.setMin(
                                Collections.min(
                                        transactions.values(),
                                        TransactionStorage::compareTransactions).getAmount());
                    }
                    if (amount.equals(transactionsStatistics.getMax())) {
                        transactionsStatistics.setMax(
                                Collections.max(
                                        transactions.values(),
                                        TransactionStorage::compareTransactions).getAmount());
                    }
                }).build();
    }

    private static int compareTransactions(Transaction transaction1, Transaction transaction22) {
        if (transaction1.getAmount() < transaction22.getAmount())
            return -1;
        else if (transaction1.getAmount() > transaction22.getAmount())
            return 1;
        return 0;
    }

    public synchronized void addTransaction(Transaction transaction) {
        if (transaction.getTimestamp() < System.currentTimeMillis() - Config.WINDOW) {
            throw new IllegalStateException();
        }
        transactions.put(UUID.randomUUID().toString(), transaction, transaction.calculateExpiryTime(), TimeUnit.MILLISECONDS);
        transactionsStatistics.update(transaction.getAmount());
    }

    public TransactionsStatistics getTransactionsStatistics() {
        return transactionsStatistics;
    }
}
