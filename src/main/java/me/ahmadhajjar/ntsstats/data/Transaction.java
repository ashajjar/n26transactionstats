package me.ahmadhajjar.ntsstats.data;

import me.ahmadhajjar.ntsstats.config.Config;

/**
 * @author Ahmad Hajjar <ash852006@gmail.com>
 * Created on 07/06/2018
 */
public class Transaction {

    private Long timestamp;
    private Double amount;

    public Transaction() {

    }

    public Transaction(Double amount, Long timestamp) {
        this.amount = amount;
        this.timestamp = timestamp;
    }

    public Long getTimestamp() {
        return timestamp;
    }

    public void setTimestamp(Long timestamp) {
        this.timestamp = timestamp;
    }

    public Double getAmount() {
        return amount;
    }

    public void setAmount(Double amount) {
        this.amount = amount;
    }

    public Long calculateExpiryTime() {
        Long offset = System.currentTimeMillis() - this.getTimestamp();
        return Config.WINDOW - offset;
    }
}
