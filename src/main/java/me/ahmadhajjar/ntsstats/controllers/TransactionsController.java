package me.ahmadhajjar.ntsstats.controllers;

import me.ahmadhajjar.ntsstats.data.Transaction;
import me.ahmadhajjar.ntsstats.data.store.TransactionStorage;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ahmad Hajjar <ash852006@gmail.com>
 * Created on 08/06/2018
 */
@RestController
public class TransactionsController {
    private final TransactionStorage transactionStorage;
    private final Logger log = LoggerFactory.getLogger(this.getClass());

    @Autowired
    public TransactionsController(TransactionStorage transactionStorage) {
        this.transactionStorage = transactionStorage;
    }

    @PostMapping(value = "/transactions", consumes = "application/json")
    public Object postTransaction(@RequestBody final Transaction transaction) {
        try {
            transactionStorage.addTransaction(transaction);
            return new ResponseEntity<Void>(HttpStatus.CREATED);
        } catch (IllegalStateException e) {
            log.debug("Transaction adding attempted but failed due to being obsolete");
            return new ResponseEntity<Void>(HttpStatus.NO_CONTENT);
        }
    }
}
