package me.ahmadhajjar.ntsstats.controllers;

import me.ahmadhajjar.ntsstats.data.store.TransactionStorage;
import me.ahmadhajjar.ntsstats.data.store.TransactionsStatistics;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * @author Ahmad Hajjar <ash852006@gmail.com>
 * Created on 08/06/2018
 */
@RestController
public class StatisticsController {
    private final TransactionStorage transactionStorage;

    @Autowired
    public StatisticsController(TransactionStorage transactionStorage) {
        this.transactionStorage = transactionStorage;
    }

    @GetMapping("/statistics")
    public TransactionsStatistics getTransactionStatistics() {
        return transactionStorage.getTransactionsStatistics();
    }

}
