package me.ahmadhajjar.ntsstats;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class NtsstatsApplication {

    public static void main(String[] args) {
        SpringApplication.run(NtsstatsApplication.class, args);
    }
}
