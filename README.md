# Transaction Statistics
## My Approach
Since this is a real time application, I decided to use something similar to what might be used in caching, an [ExpiringMap](https://github.com/jhalterman/expiringmap) which is similar to [Google Collections](https://github.com/google/guava) except for the fact that google solution is not flexisble enough to provide expiry time for each and every item in the map.

So basically the application works as follows: 

1. Once the application starts it will load the `TransactionStorage` class which in turn initiates the underlaying storage of the transactions, represented by an `ExpiringMap` which has a `String` as its key, and an object of type `Transaction` as its value. The key for now is an auto generated `UUID`. Istantiating this map includes:

	* Setting a maximum size. For now this is set to 60000, which means, in theory, the application can take up to one transaction per millisecond, of course this can be set to what the frequency of transactions is assumed to be. 
	* Enabling variable expiry time. To allow for setting individual expiry times.
	* And most importantly, setting expiry callback, which will be responsible for updating the transactions' stats after a transaction has expired and removed.

2. Everytime the endpoint for adding transactions is called, it will check if the transaction is valid, and it will add it to the transactions map, and update the stats represented by an object of type `TransactionsStatistics`, otherwise it will throw an `IllegalStateException`.
3. Everytime the endpoint of stats is called, it will just get the statistics object of type `TransactionsStatistics`

**This approach, will provide `O(1)` space and time for both endpoints, but if I were a solution architect in N26 I would be more interested in higher scalability and availability solutions, something cloud-based 🌩 😁**

## Alternative Approaches

1. Using a distributed in-memory database like [Redis](https://redis.io/).
2. Using a streaming solution along with some queuing system. Which is what I think is the best approach. If I had more time I would have done that instead of writing a small java application.

### Straming Solution High Level Desgin

The problem propsed in this challenge is a well-known challenge, that has well known solutions. The famous name of this problem is real-time event processing. And is usually solved using straming solutions like [Apache Storm](http://storm.apache.org/) or [Apache Flink](https://flink.apache.org/) ..etc.

The following solution depends on Kafka and Storm.

![High Level Block Diagram](diagram.png)

The main components would be: 

1. REST interface, SpringBoot app that would push transactions to Kafka.
2. [Apache Kafka](https://kafka.apache.org/) is another streaming solution but here will be used as a distributed queue solution, that would act like the integration platform between the REST interface and Apache Storm.
3. Apache Storm is the core component of the system, it will be the processing engine. That will be done by implementing a sliding window, real-time, event processing flow.